// Yatsenko Maryna SE-3

const express = require('express')
const bodyParser = require('body-parser');


const app = express()


const PORT = process.env.PORT || 56201;

//app.use(express.json());
app.use(bodyParser.text());

app.post('/square', (req, res) => {    console.log(req.body);

   // Перевіряємо заголовок - чи дійсно нам надійшов звичайний текст
    if (req.get('Content-Type') !== 'text/plain') {
        return res.status(400).json({ error: 'Invalid Content-Type. Expected text/plain.' });
    }


    const num = +req.body;

    // Валідація числа
    if (isNaN(num)) {
        return res.status(400).json({ error: 'Invalid number provided' });
    }

    const result = num * num;

    // Встановлення заголовку як JSON
    res.setHeader('Content-Type', 'application/json');
    return res.json({
        number: num,
        square: result
    });


})


app.post('/reverse', (req, res) => {    console.log(req.body);

    // Перевіряємо заголовок - чи дійсно нам надійшов звичайний текст
    if (req.get('Content-Type') !== 'text/plain') {
        return res.status(400).json({ error: 'Invalid Content-Type. Expected text/plain.' });
    }


    const text = req.body;

    let reversedText;
    try {
        reversedText = text.split('').reverse().join('');
    } catch (error) {
        return res.status(500).json({ error: 'Error while reversing text.' });
    }


    // Встановлення заголовку як звичайний текст
    res.setHeader('Content-Type', 'text/plain');
    res.send(reversedText);



})



app.get('/date/:year/:month/:day', (req, res) => {    console.log(req.body);

    const { year, month, day } = req.params;

    const paddedMonth = month.padStart(2, '0');
    const paddedDay = day.padStart(2, '0');


    const isLeapYear = (year % 4 === 0 && year % 100 !== 0) || (year % 400 === 0);

    const date = new Date(year, paddedMonth - 1, paddedDay);
    const currentDate = new Date();
    let difference = Math.abs(Math.ceil((date - currentDate) / (1000 * 60 * 60 * 24)));

   // if (date > currentDate) {
    //    difference += 1;
   // }

    const weekDays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    const weekDay = weekDays[date.getDay()];

    // Встановлення заголовку як JSON
    res.setHeader('Content-Type', 'application/json');

    res.json({
        weekDay,
        isLeapYear: isLeapYear,
        difference
    });



})



app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
    //Дослідіть константи середовища, котрі знаходяться у об'єкті process.env
    console.log(process.env);
});

